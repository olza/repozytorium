#include <stdio.h>
#include <stdlib.h>
#include "funkcje.h"

void wczytaj(FILE *plik, float *p_kolX, float *p_kolY, float *p_kolRHO);

int main()
{
    float *kol_X;
    float *kol_Y;
    float *kol_RHO;

   kol_X = (float*)malloc(50 * sizeof(float));
   kol_Y = (float*)malloc(50 * sizeof(float));
   kol_RHO = (float*)malloc(50 * sizeof(float));


    FILE * plik;
    if((plik = fopen("P0001_attr.rec", "a+"))==NULL)
        printf("Blad w otwarciu pliku!")    
    else
    {
        wczytaj(plik, kol_X, kol_Y, kol_RHO);
        fprintf(plik, "Srednia arytmetyczna wartosci z kolumy X wynosi: %f", srednia(kol_X));
        fprintf(plik, "Mediana wartosci z kolumny X wynosi: %f", mediana(kol_X));
        fprintf(plik, "Odchylenie standardowe wartosci z kolumy X wynosi: %f", odchylenie(kol_X));

        fprintf(plik, "Srednia arytmetyczna wartosci z kolumy Y wynosi: %f", srednia(kol_Y));
        fprintf(plik, "Mediana wartosci z kolumny Y wynosi: %f", mediana(kol_Y));
        fprintf(plik, "Odchylenie standardowe wartosci z kolumy Y wynosi: %f", odchylenie(kol_Y));

        fprintf(plik, "Srednia arytmetyczna wartosci z kolumy RHO wynosi: %f", odchylenie(kol_RHO));
        fprintf(plik, "Mediana wartosci z kolumny RHO wynosi: %f", mediana(kol_RHO));
        fprintf(plik, "Odchylenie standardowe wartosci z kolumy RHO wynosi: %f", odchylenie(kol_RHO));

    }
        printf(plik, "Srednia arytmetyczna wartosci z kolumy X wynosi: %f", srednia(kol_X));
        printf(plik, "Mediana wartosci z kolumny X wynosi: %f", mediana(kol_X));
        printf(plik, "Odchylenie standardowe wartosci z kolumy X wynosi: %f", odchylenie(kol_X));

        printf(plik, "Srednia arytmetyczna wartosci z kolumy Y wynosi: %f", srednia(kol_Y));
        printf(plik, "Mediana wartosci z kolumny Y wynosi: %f", mediana(kol_Y));
        printf(plik, "Odchylenie standardowe wartosci z kolumy Y wynosi: %f", odchylenie(kol_Y));

        printf(plik, "Srednia arytmetyczna wartosci z kolumy RHO wynosi: %f", odchylenie(kol_RHO));
        printf(plik, "Mediana wartosci z kolumny RHO wynosi: %f", mediana(kol_RHO));
        printf(plik, "Odchylenie standardowe wartosci z kolumy RHO wynosi: %f", odchylenie(kol_RHO));

    fclose(plik);

    free(kol_X);
    free(kol_Y);
    free(kol_RHO);

    return 0;
}
void wczytaj(FILE *plik, float *p_kolX, float *p_kolY, float *p_kolRHO)
{
    char lp[4] = "";
    int i = 0;

    while(fscanf(plik, "%s %f %f %f", lp, &p_kolX[i], &p_kolY[i], &p_kolRHO[i])>0)
        {
            printf("%s %f %f %f \n", lp, p_kolX[i], p_kolY[i], p_kolRHO[i]);
            i++;
        }

    return;
}


